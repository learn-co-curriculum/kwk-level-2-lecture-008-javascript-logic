# Logic, Arrays, and Objects

**NOTE:** This is another large lecture. Make sure to take frequent breaks, show every example in code in front of the students, and encourage the students to both ask and answer questions as you are moving through. Do your best to, whenever a question is asked that can be expressed in code, you prove the provided answer with a code example.

## Objectives

This lecture seeks to further explore fundamentals of the JavaScript language. It will introduce logical operators in JavaScript, and use examples of different operators and conditions. Additionally, objects, and their use as arrays, will be introduced.

## SWBATS

+ JAVASCRIPT - use logical operators
+ JAVASCRIPT - write `if`/`else` statements using logical operators
+ JAVASCRIPT - create and manipulate arrays and objects

## INTRODUCTION

JavaScript is a significant programming language that has evolved over the
years. Understanding this, don't let its size intimidate you. With just some of
the basics and a little patience, you can do all sorts of things, including:
creating awesome interactive websites, web games, and even Chrome extensions.
With a bit more JS practice, you can create your own chatrooms, create augmented
reality experiences, get real-time data notifications, and even write basic
_learning machines_. 

We're not there yet though.  We've got a few more core concepts to cover on
introductory JavaScript.  Almost everything in JavaScript is built off of what
we're learning today, so once you've learned these concepts and understand
them, you absolutely can build whatever you put your mind towards.

In this lecture we're going to be talking a bit about logic and conditionals,
dive into some data types we haven't covered yet, arrays and objects, and finish
up with one super powerful concept - _loops_.

## Logical Operators

Operators are symbols that represent things like basic mathematic concepts or
comparison between values. We've seen some operators already:

* **+**  - the plus sign represents addition
* **-**  - the dash represents subtraction
* __*__  - the asterisk represents multiplication
* **/**  - the slash represents division

We've also see this one: **=**, the equals symbol. This symbol, however, **does not mean equals**.  It is the JavaScript operator for _assignment_. We've seen this:

```js
var myName = "Adele"
```

On this line, we're telling JavaScript _assign_ the value "Adele" to the
variable called `myName`. Remember: **If you use one equals sign, JS will try
to assign whatever is on the right side of the symbol to whatever is on the
left side of the symbol**.

For now, let's focus namely on the equality and relational operators. These
symbols are useful for _comparing_ values instead of just assigning them.

### Equality and Relational operators

Using equality operators allows us to compare to values or variables
representing values.  These statements always return a _boolean_ value, `true` or
`false`.

#### `===`

The **strict equality operator**, `===`, returns `true` if two values are
exactly equal. The `===` operator will only return `true` if the data types and
values match:

```js
42 === 42 // both 42 and 42 are numbers and are equal
// > true

42 === "42" // 42 is a number, but "42" is a string
// > false

true === 1 // true is a boolean, but 1 is a number
// > false

'0' === false // "0" is a string, but false is a boolean
// > false

null === undefined  // JavaScript considers these two different things
// > false

' ' === 0 // " " is a string, but 0 is a number
// > false

var myVariable = 5
5 === myVariable // 5 is a number, and myVariable represents the number 5, so this is true
// > true
```

#### `!==`

The **strict inequality operator** returns `true` if two values are _not_ equal
and does not perform type conversions:

```js
9000 !== 9001
// > true

9001 !== '9001'
// > true

[] !== ''
// > true
```

#### `>`, `<`, `>=`, `<=`

The **greater than** (`>`), **greater than or equals** (`>=`), **less than** (`<`), and **less than or equals** (`<=`) operators are pretty self-explanatory:

```js
88 > 9
// > true

88 < 9
// > false

88 > 88
// > false

88 >= 88
// > true

88 <= 9
// > false
```

Beware of type conversion when comparing non-numbers against numbers. For
instance, when a string is compared with a number, the JavaScript engine tries
to convert the string to a number, so this returns true:

```js
88 > "9"
// > true
```

**NOTE:** it's worthwhile to mention the `==` equality operator so students aren't thrown off when they see it in code elsewhere. Make note that, for all intensive purposes, `==` is less strict and `===` should be used as default. An easy example is to show how `==` and `===` behave with `1` and `'1'`.

## Conditional Statements

Okay. So we've covered some conditional operators.. but how can we use them?
_Well_, one of the most common uses of these comparison operators is in
_conditional statements_.  In the previous lecture, when we were discussing data
types, we talked about a variable `isRaining`:

```js
var isRaining = true
```

Say we were headed out of the house to go to school... if this variable is true,
we grab an umbrella and boots.

We can actually write this programmatically using a conditional statement:

```js
var isRaining = true
if (isRaining) {
  // it's raining! get the umbrella and your boots
}
```

Conditional statements are given an argument, in this case `isRaining`, and if
that argument is "truthy" or `true`, will execute the code inside the set of curly
braces.  If the argument isn't truthy , this code is skipped over.  We can
also bring in our comparison operators, since they will always return `true` or
`false`:

```js
var age = 30
var canVote = false

canVote
// > false

if (age >= 18) {
  canVote = true //will ONLY be assigned true if age variable is greater than or equal to 18
}

canVote
// > true
```

Here, the `canVote` variable is `false` to start.  JS then hits the conditional
statement, the argument given, `age >= 18` evaluates to `true`, so `canVote` is
then switched! In this way, we can build specific actions based on whether or
not some statement, like `age >= 18`, is `true`.

What if we wanted to do one thing _or another_? We can expand our conditional
statement so it will not only execute code if the argument given evaluates to
`true`, but will also fire different code if it is `false`. To do this, we add
and `else { }` section:

```js
var isRaining = true
if (isRaining) {
  //it's raining! get the umbrella and your boots
} else {
  //it's not raining! where are my flip flops?
}
```

To expand on our voting example:

```js
var age = 16 //testing a different age
var canVote = false

canVote
// > false

if (age >= 18) {
  canVote = true //will ONLY be assigned true if age variable is greater than or equal to 18
} else {
  console.log('You cannot vote yet')
}

canVote
// > false still
```

In the above example, the age is now 16.  When JS hits the conditional
statement, `age >= 18` evaluates to _false_, triggering the `else` statement and
logging the message 'You cannot vote yet' in the console.  This message will
only ever be seen if the `else` statement is called.   

We can nest conditionals within each other. So for example:

```js
var age = 16 //testing a different age
var canVote = false
var canDrive = false

canVote
// > false

if (age >= 18) {
  //if 18 or older, you can both drive and vote
  canVote = true
  canDrive = true

} else {
  //if under 18, you can't vote, so canVote stays false

  if (age >= 16) {
      //but! if 16 or older, you can drive
      canDrive = true
  }
  console.log('You cannot vote yet')
}

canVote
// > false still
canDrive
// > true, since age is 16
```

In this way, we can set multiple possible conditions.  Under 16, we can't vote
or drive, over 18, we can. Under 18, _but over 16_, we _can_ drive.   

#### `else if`

Another way to represent multiple possible conditions is with `else if` clauses:

```js
var age = 17
var canVote = false
var canDrive = false

if (age >= 18) {
  canVote = true
  canDrive = true
} else if (age >= 16) {
  canDrive = true
} else {
  console.log('You cannot vote or drive yet')
}
```

Here, if the first condition is true, the code in the first set of braces is
called, and both `canVote` and `canDrive` are set to true.  If that condition
is _false_, the `else if` condition is tested.  If _that_ condition is true,
only `canDrive` is set to true. If neither condition is true, the
`console.log()` kicks in and logs the message.

**NOTE:** Writing conditionals in console can be frustrating because you must hold down
`shift` when trying to write multiple lines of code in order to prevent it from
trying to fire the code before you're finished writing. Instead, consider writing your code in text files, then running them with `node <filename>` in bash. 

We'll come back to conditionals very soon, but before then, we will change
gears.

The remainder of this lecture will be focused on some special data types we
haven't talked about yet: arrays and objects, and some of the wonderful things
we can do with them.

## Arrays

It's time to talk about data structures! A data structure is a way to
associate and organize information. Outside of the programming world, we use
data structures all the time. For example, we might have a shopping list of the
items we need to buy at the mall or an address book for organizing contact
information.

If we have a lot of related data, it wouldn't make sense to represent it in an
unrelated system. Imagine that we're working on a lottery application that has
to represent the winning lottery numbers. We could do that as follows:

```js
var firstNumber = 32
var secondNumber = 9
var thirdNumber = 14
var fourthNumber = 33
var fifthNumber = 48
var powerBall = 5
```

We've represented all six pieces of data, but they aren't related in any
meaningful way. Every single time we want to reference that combination of
winning numbers, we need to remember and type out six different variable names:

```js
var firstNumber = 32
var secondNumber = 9
var thirdNumber = 14
var fourthNumber = 33
var fifthNumber = 48
var powerBall = 5

function logWinningNumbers (first, second, third, fourth, fifth, power) {
  console.log('Winning numbers:', first, second, third, fourth, fifth, power)
}

logWinningNumbers(firstNumber, secondNumber, thirdNumber, fourthNumber, fifthNumber, powerBall)
// > Winning numbers: 32 9 14 33 48 5
```

That's so much typing! There are much, much better ways to keep data organized
in JavaScript. One of the most common is the array.

### Creating arrays

An array is an ordered list surrounded by square brackets `[]`:

```js
var arr = ['This', 'is', 'an', 'array', 'of', 'strings.']
// > ["This", "is", "an", "array", "of", "strings."]
```

The members or elements in an array can be data of any type:

```js
var arr = ['Hello, world!', 42, null, NaN, []]
// > ["Hello, world!", 42, null, NaN, []]
```

Arrays are ordered, meaning that the elements in them will always appear in the
same order. The array `[1, 2, 3]` is different from the array `[3, 2, 1]`.

```js
var primeNumbers = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]

var tvShows = ['Game of Thrones', 'True Detective', 'The Good Wife', 'Empire']
```

We can find out how many elements an array contains by checking the array's
built-in `length` property:

```js
var myArray = ['This', 'array', 'has', 5, 'elements']

myArray.length
// > 5
```

To get a sense of just how awesome arrays are at keeping data organized, let's
rewrite our lottery code to use an array:

```js
var winningNumbers = [32, 9, 14, 33, 48, 5]

function logWinningNumbers (numbers) {
  console.log('Winning numbers:', numbers)
}

logWinningNumbers(winningNumbers)
// > Winning numbers: [32, 9, 14, 33, 48, 5]
```

So much simpler, and we only have to remember one identifier
(`winningNumbers`) instead of six (`firstNumber`, `secondNumber`, and so on).

The one benefit of storing all six lottery numbers separately is that we had a
really easy way to access each individual number. For example, we could just
reference `powerBall` to grab the sixth number. Luckily, arrays offer an equally
simple syntax for accessing individual members.

## Accessing and changing elements in an array

Every element in an array is assigned a unique index value that corresponds to
its place within the collection. The first element in the array is at index `0`,
the fifth element at index `4`, and the 428th element at index `427`. Arrays are
present in most computer languages, and most agree that arrays indexes start at
`0`. Say goodbye to the world where counting started at one, you are programmers
now. 

To access an element in an array, we _access by index_, which conveniently
enough looks exactly like an array:

```js
var winningNumbers = [32, 9, 14, 33, 48, 5]
// > undefined

winningNumbers[0] // the first element in the array
// > 32

winningNumbers[4] // the fifth element in the array
// > 48
```

Let's take a minute to think about how we could access the **last** element in
any array.

If `myArray` contains 10 elements, the final element will be at `myArray[9]`. If
`myArray` contains 15000 elements, the final element will be at
`myArray[14999]`. So the index of the final element is always one less than the
number of elements in the array. If only we had an easy way to figure out how
many elements are in the array...

Oh yeah!

```js
var alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
// > undefined

alphabet.length
// > 26

alphabet[alphabet.length - 1]
// > "z"
```

**NOTE:** Query the students -- why do we need the `- 1`? What happens if we try to access by index `26`? Run it in Node.

### Adding elements to an array

JavaScript allows us to manipulate the members in an array in a number of ways.

### `.push()` and `.unshift()`

With the `.push()` method, we can add elements to the end of an array:

```js
var superheroes = ['Catwoman', 'She-Hulk', 'Jessica Jones']

superheroes.push('Wonder Woman')

superheroes
// > ["Catwoman", "She-Hulk", "Jessica Jones", "Wonder Woman"]
```

We can also `.unshift()` elements onto the beginning of an array:

```js
var cities = ['New York', 'San Francisco']

cities.unshift('Los Angeles')

cities
// > ["Los Angeles", "New York", "San Francisco"]
```

### `.pop()` and `.shift()`

The `.pop()` method removes the last element in an array, destructively updating
the original array:

```js
var days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

days.pop()
// > "Sun"

days
// > ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
```

The `.shift()` method removes the first element in an array, also mutating the
original:

```js
var days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

days.shift()
// > "Mon"

days
// > [Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
```

Notice that the return value for the `.pop()` and `.shift()` methods is the
element that was removed.

**NOTE:** Student should break off into small groups and practice what they've learned. They can create arrays, try to add to them in the console, try to read specific element in an array.

## Objects

Arrays are awesome for representing simple, ordered data sets, but they're
generally not great at modeling any sort of complex structure. For example,
let's think about how we could represent a mailing address.

Addresses are made up of words and numbers, so at first it might make sense to
store the address as a string:

```js
var address = '11 Broadway, 2nd Floor, New York, NY 10004'
```

That looks decent enough, but what happens if the company moves to a different
floor in the same building? We just need to modify one piece of the address,
but with a string we'd have to involve some pretty complicated find-and-replace
pattern matching or simply replace the entire thing. Instead, let's throw the
different pieces of the address into an array:

```js
var address = ['11 Broadway', '2nd Floor', 'New York', 'NY', 10004]
```

Now, we can just grab the small piece that we want to update and leave the rest
as-is:

```js
address[1] = '3rd Floor'

address
// > ["11 Broadway", "3rd Floor", "New York", "NY", 10004]
```

This seems like a better solution, but it still has its drawbacks. Namely,
`address[1]` is a sub-optimal way to refer to the second line of an address.
What if there is no second line, e.g., `['11 Broadway', 'New York', 'NY',
10004]`? Then `address[1]` will contain the city name instead of the floor
number. `address[1]` offers very little insight into what data we should expect
to find in there. It's a part of an address, sure, but which part?

To get around this, we could store the individual pieces of the address in
separate, appropriately-named variables:

```js
var street1 = '11 Broadway'
var street2 = '2nd Floor'
var city = 'New York'
var state = 'NY'
var zipCode = 10004
```

That's solved one issue but reintroduced the same problem we tackled in the
section on arrays: storing pieces of related data in a bunch of unrelated
variables is not a great idea! If only there were a best-of-both-worlds solution —
a way to store all of our address information in a single data structure while
also maintaining a descriptive naming scheme...

Well, the amazing data structure we're after is the ***object***.

### What is an object?

A JavaScript object is a collections of properties. We create them bound with curly braces `{ }`.
The properties can point to values of any data type — even other objects.

We can have empty objects:

```js
var myObj = {}
```

Or objects with a single _key-value_ pair:

```js
var myObj = { key: value }
// i.e      { street: "Broad Street" }
```

When we have to represent multiple key-value pairs in the same object (which is most of the time), we use commas to separate them out:

```js
var volunteer = {
  firstName: "Katniss"
  lastName: "Everdeen"
}
```

Let's circle back on our address example:

```js
var address = {
  street1: '11 Broadway',
  street2: '2nd Floor',
  city: 'New York',
  state: 'NY',
  zipCode: 10004
}
```

The real data in an object is stored in the _value_ half of the key-value
pairings. The _key_ is what lets us access that value. In the same way we name
variables and functions, inside an object we assign each value a key. We can
then refer to that key and the JavaScript engine knows exactly which value we're
trying to access.  

### Accessing values in an object

There are two ways to access values in an object, one of which we already
learned about in the arrays lesson.

### Dot notation

With _dot notation_, we use the _member access operator_ (a single period) to
access values in an object. For example, we can grab the individual pieces of
our address, above, as follows:

```js
address.street1
// > "11 Broadway"

address.street2
// > "2nd Floor"

address.city
// > "New York"

address.state
// > "NY"

address.zipCode
// > 10004
```

Dot notation is fantastic for readability, as we can just reference the key name
(e.g., `street1` or `zipCode`). Because of this simple syntax, it should be your
go-to strategy for accessing the properties of an object.

**NOTE**: Most people just call it _dot notation_ or the _dot operator_, so
don't worry too much about remembering the term _member access operator_.

#### Accessing nonexistent properties

If we try to access the `country` property of our `address` object, what will
happen?

```js
address.country
// > undefined
```

It returns `undefined` because there is no matching key on the object.
JavaScript is too nice to throw an error, so it lets us down gently. Keep this
in mind, though: if you're seeing `undefined` when trying to access an object's
properties, it's a good indicator to recheck which properties exist on the
object (along with your spelling and capitalization)!

### Bracket notation

With _bracket notation_, we use a pair of square brackets `[]`. To access
the same properties as above, we need to represent them as strings inside the
operator:

```js
address['street1']
// > "11 Broadway"

address['street2']
// > "2nd Floor"

address['city']
// > "New York"

address['state']
// > "NY"

address['zipCode']
// > 10004
```


Bracket notation is a bit harder to read than dot notation, so we always default
to the former. However, there are times when it is useful, like when the key is
a string with _spaces_ in it, or when have a variable representing a key, like below:

```js
var mySpecialKey = 'street2'

address[mySpecialKey]
// > "2nd Floor"
```

You can create specific key value pairs by assigning a value to a specific key within an object.  If that key doesn't exist, it will be created to store the value you've given.  You can do this in either notation like so:

```js
//both of these are valid ways to create a key, 'country' with the value 'United States'
address.country = "United States"
address['country'] = "United States"
```

Objects are way cool.  They can even store _functions as values_ in their
key/value pairs.  Objects end up being very useful in all sorts of situations.

## Moving Forward

Phew! That was a lot.  Let's do a quick recap of what we've learned

**Comparison Operators**:
  - allow us to compare if two values are equal, greater than, or less than each other
  - return `true` or `false` values
  - are useful in conditional statements

**Conditional Statements**:
  - allow us to fire code _only_ if a certain criteria is met
  - can run code if a certain criteria is _true_, as well as different code if
  the criteria is _false_
  - can be nested within each other (or you can use `else if`), allowing multiple criteria to be used to produce more than two outcomes

**Arrays**:
  - are collections of _ordered_ information/values
  - allow many values to be stored in one variable
  - can be accessed by referring to the index of a value, starting at 0
  - can be added to with `push()` and `unshift()`
  - can have elements removed with `shift()` and `pop()`

**Objects**:
  - are collections of data, organized in _key/value_ pairs
  - values in objects can be accessed by writing the objects name, followed by `.` and then the appropriate _key_, or by writing the key (as a string) within brackets, similar to Arrays
